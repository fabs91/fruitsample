# FruitSample
List some fruits

## Technologies used
The app is based on the **ModelView-ViewModel** pattern.

Java 8, Kotlin, Android support, Dagger 2, Retrofit 2, RxJava 2, JUnit, Mockito

