package com.test.fruitsample.homepage.homepage

import com.test.fruitsample.analytics.AnalyticsReporter
import com.test.fruitsample.service.FruitsServiceImpl
import com.test.fruitsample.service.model.Fruit
import com.test.fruitsample.service.net.FruitsApiService
import com.test.fruitsample.service.net.response.FruitsReponse
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FruitsServiceTest {

    @Mock
    private lateinit var fruitsApiService: FruitsApiService

    @Mock
    private lateinit var analyticsReporter: AnalyticsReporter

    @InjectMocks
    lateinit var fruitsService: FruitsServiceImpl

    private lateinit var testObserver: TestObserver<List<Fruit>>

    @Test
    fun getFruitsWhenSuccess() {
        `when`(fruitsApiService.getFruits()).thenReturn(Single.just(FruitsReponse(fruits = emptyList())))

        testObserver = fruitsService.getFruits().test()

        verify(analyticsReporter).initEvent()
        verify(analyticsReporter).sendLoadEvent()
        verify(analyticsReporter, never()).sendErrorEvent(ArgumentMatchers.anyString())
    }

    @Test
    fun getFruitsWhenError() {
        `when`(fruitsApiService.getFruits()).thenReturn(Single.error(NullPointerException()))

        testObserver = fruitsService.getFruits().test()

        verify(analyticsReporter).sendErrorEvent(ArgumentMatchers.anyString())
    }

}

