package com.test.fruitsample.homepage.analytics

import com.test.fruitsample.analytics.AnalyticsReporter
import com.test.fruitsample.service.net.FruitsApiService
import com.test.fruitsample.util.TimeProvider
import io.reactivex.Completable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AnalyticsReporterTest {

    @Mock
    private lateinit var timeProvider: TimeProvider

    @Mock
    private lateinit var apiService: FruitsApiService

    @InjectMocks
    private lateinit var analyticsReporter: AnalyticsReporter

    @Test
    fun sendLoadEventWhenEventInit() {
        val initTime = 1000L
        val lastTime = 3000L
        `when`(timeProvider.getTimeInMs()).thenReturn(initTime)
        `when`(timeProvider.getCurrentTimeDiff(initTime)).thenReturn(lastTime)

        `when`(apiService.sendStats(AnalyticsReporter.LOAD_EVENT, lastTime.toString())).thenReturn(Completable.complete())

        analyticsReporter.initEvent()
        analyticsReporter.sendLoadEvent()

        verify(apiService).sendStats(AnalyticsReporter.LOAD_EVENT, lastTime.toString())
    }

    @Test
    fun sendDisplayEventWhenEventInit() {
        val initTime = 1000L
        val lastTime = 3000L
        `when`(timeProvider.getTimeInMs()).thenReturn(initTime)
        `when`(timeProvider.getCurrentTimeDiff(initTime)).thenReturn(lastTime)

        `when`(apiService.sendStats(AnalyticsReporter.DISPLAY_EVENT, lastTime.toString())).thenReturn(Completable.complete())

        analyticsReporter.initEvent()
        analyticsReporter.sendDisplayEvent()

        verify(apiService).sendStats(AnalyticsReporter.DISPLAY_EVENT, lastTime.toString())
    }

    @Test
    fun sendErrorEventWhenEventInit() {
        val message = "ERROR"
        `when`(apiService.sendStats(AnalyticsReporter.ERROR_EVENT, message)).thenReturn(Completable.complete())

        analyticsReporter.sendErrorEvent(message)

        verify(apiService).sendStats(AnalyticsReporter.ERROR_EVENT, message)
    }

    @Test
    fun sendNoEventWhenEventNotInit() {
        val lastTime = 3000L
        analyticsReporter.sendLoadEvent()

        verify(apiService, never()).sendStats(AnalyticsReporter.LOAD_EVENT, lastTime.toString())
    }
}

