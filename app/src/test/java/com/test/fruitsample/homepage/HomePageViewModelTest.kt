package com.test.fruitsample.homepage.homepage

import com.test.fruitsample.homepage.HomePageViewModel
import com.test.fruitsample.homepage.model.FruitModel
import com.test.fruitsample.service.FruitsService
import com.test.fruitsample.service.model.Fruit
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomePageViewModelTest {

    @Mock
    private lateinit var fruitsService: FruitsService

    @InjectMocks
    lateinit var homePageViewModel: HomePageViewModel

    private lateinit var testObserver: TestObserver<MutableList<FruitModel>>

    @Test
    fun getFruitsWhenSuccess() {
        val fruits = mutableListOf<Fruit>()
        val type = "orange"
        val price = 10
        val weight = 100
        fruits.add(Fruit(type, price, weight))
        `when`(fruitsService.getFruits()).thenReturn(Single.just(fruits))

        testObserver = homePageViewModel.getFruits().test()

        testObserver.assertValueAt(0, {
            val fruitModel = it.get(0)
            fruitModel.type.equals(type) && fruitModel.price == price && fruitModel.weight == weight
        })
    }

    @Test
    fun getFruitsWhenError() {
        `when`(fruitsService.getFruits()).thenReturn(Single.error(NullPointerException()))

        testObserver = homePageViewModel.getFruits().test()

        testObserver.assertError(NullPointerException::class.java)
    }

}

