package com.test.fruitsample.di.application

import android.content.Context
import com.test.fruitsample.FruitSampleApp
import com.test.fruitsample.analytics.AnalyticsReporter
import com.test.fruitsample.di.SchedulersModule
import com.test.fruitsample.service.net.FruitsApiService
import com.test.fruitsample.service.net.NetworkModule
import com.test.fruitsample.util.TimeProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [SchedulersModule::class, NetworkModule::class])
class ApplicationModule internal constructor(private val application: FruitSampleApp) {

    @Singleton
    @Provides
    internal fun provideApplicationContext(): Context {
        return application.getApplicationContext()
    }

    @Singleton
    @Provides
    internal fun provideApplication(): FruitSampleApp {
        return application
    }

    @Singleton
    @Provides
    internal fun provideTimeProvider(): TimeProvider {
        return TimeProvider()
    }

    @Singleton
    @Provides
    internal fun provideAnalyticsReporter(timeProvider: TimeProvider, fruitsApiService: FruitsApiService): AnalyticsReporter {
        return AnalyticsReporter(timeProvider, fruitsApiService)
    }

}