package com.test.fruitsample.di.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.test.fruitsample.FruitSampleApp

/**
 * Base Activity that is used to help ensure the correct workflow is followed for creating the
 * Dagger DI graph
 */
abstract class BaseActivity : AppCompatActivity() {

    private var component: ActivityComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component = createActivityComponent(application as FruitSampleApp)
        injectDependencies(component)
    }

    override fun onDestroy() {
        component = null
        super.onDestroy()
    }

    /**
     * Create the Activity's component. This needs to be created by the Activity itself as creating
     * it in this parent class would mean the Activity's dependencies would not be visible.
     *
     * @param application the application which contains the dependency graph
     * @return The new Activity component that will live for the lifespan of the Activity
     */
    protected abstract fun createActivityComponent(application: FruitSampleApp): ActivityComponent

    /**
     * Call inject on all the classes which require fields injection
     *
     * @param component The component that was created using
     */
    protected abstract fun injectDependencies(component: ActivityComponent?)

}