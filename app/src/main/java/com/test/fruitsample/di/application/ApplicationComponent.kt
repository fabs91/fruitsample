package com.test.fruitsample.di.application

import com.test.fruitsample.FruitSampleApp
import com.test.fruitsample.details.FruitDetailsComponent
import com.test.fruitsample.details.FruitDetailsModule
import com.test.fruitsample.di.ServicesModule
import com.test.fruitsample.homepage.HomePageComponent
import com.test.fruitsample.homepage.HomePageModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, ServicesModule::class))
interface ApplicationComponent {

    //Subcomponents inherit and extend the object graph of the parent component
    //useful to create subgraphs to wrap different parts of the app
    fun plusHome(module: HomePageModule): HomePageComponent

    fun plusDetails(module: FruitDetailsModule): FruitDetailsComponent

    fun inject(app: FruitSampleApp)

}
