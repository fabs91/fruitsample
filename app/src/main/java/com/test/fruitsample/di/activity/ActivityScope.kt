package com.test.fruitsample.di.activity

import javax.inject.Scope

@Scope
annotation class ActivityScope