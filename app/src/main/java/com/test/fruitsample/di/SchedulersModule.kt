package com.test.fruitsample.di

import com.test.fruitsample.di.qualifier.IoScheduler
import com.test.fruitsample.di.qualifier.MainScheduler
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class SchedulersModule {

    @Provides
    @Singleton
    @MainScheduler
    fun provideUiScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Singleton
    @IoScheduler
    fun provideIOScheduler(): Scheduler {
        return Schedulers.io()
    }
}