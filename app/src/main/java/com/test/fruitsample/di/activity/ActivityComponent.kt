package com.test.fruitsample.di.activity

import dagger.Subcomponent

/**
 * Base component for Activities
 * Each sub component activity will extends this base class to have common modules
 */
@Subcomponent(modules = [(ActivityModule::class)])
interface ActivityComponent