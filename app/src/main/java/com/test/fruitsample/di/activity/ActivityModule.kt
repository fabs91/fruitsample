package com.test.fruitsample.di.activity

import dagger.Module
import dagger.Provides


@Module
open class ActivityModule(private val activity: BaseActivity) {

    @Provides
    @ActivityScope
    fun provideActivity(): BaseActivity {
        return activity
    }

}