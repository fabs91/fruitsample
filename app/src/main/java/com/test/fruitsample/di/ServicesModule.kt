package com.test.fruitsample.di

import com.test.fruitsample.service.FruitsService
import com.test.fruitsample.service.FruitsServiceImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ServicesModule {

    @Binds
    internal abstract fun bindFruitsService(fruitsServiceImpl: FruitsServiceImpl): FruitsService

}