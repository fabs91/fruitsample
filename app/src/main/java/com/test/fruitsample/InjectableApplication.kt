package com.test.fruitsample

import com.test.fruitsample.details.FruitDetailsActivity
import com.test.fruitsample.details.FruitDetailsComponent
import com.test.fruitsample.homepage.HomePageActivity
import com.test.fruitsample.homepage.HomePageComponent

interface InjectableApplication {

    fun createHomePageComponent(homePageActivity: HomePageActivity): HomePageComponent

    fun createFruitDetailsComponent(detailsFruitActivity: FruitDetailsActivity): FruitDetailsComponent

}