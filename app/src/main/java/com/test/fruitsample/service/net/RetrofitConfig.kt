package com.test.fruitsample.service.net

import okhttp3.OkHttpClient

class RetrofitConfig(var client: OkHttpClient, var baseUrl: String)
