package com.test.fruitsample.service

import com.test.fruitsample.service.model.Fruit
import io.reactivex.Single

interface FruitsService {
    fun getFruits(): Single<List<Fruit>>
}