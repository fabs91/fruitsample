package com.test.fruitsample.service.net

import com.test.fruitsample.BuildConfig
import com.test.fruitsample.di.qualifier.IoScheduler
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class NetworkModule {

    @Singleton
    @Provides
    internal fun provideRetrofitConfig(logging: HttpLoggingInterceptor): RetrofitConfig {
        return RetrofitConfig(OkHttpClient.Builder()
                .addInterceptor(logging)
                .build(),
                BuildConfig.BASE_API_URL)
    }

    @Provides
    internal fun provideRetrofit(retrofitConfig: RetrofitConfig,
                                 @IoScheduler scheduler: Scheduler): Retrofit {
        return Retrofit.Builder().baseUrl(retrofitConfig.baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(scheduler))
                .addConverterFactory(GsonConverterFactory.create())
                .client(retrofitConfig.client)
                .build()
    }

    @Provides
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        }
        return logging
    }

    @Singleton
    @Provides
    internal fun provideFruitsApiRetrofitService(retrofit: Retrofit): FruitsApiRetrofitService {
        return retrofit.create(FruitsApiRetrofitService::class.java)
    }

}