package com.test.fruitsample.service.net

import com.test.fruitsample.service.net.response.FruitsReponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface FruitsApiRetrofitService {
    @GET("data.json")
    fun getFruits(): Single<FruitsReponse>

    @GET("stats")
    fun sendStats(@Query("event") event: String, @Query("data") data: String): Completable

}