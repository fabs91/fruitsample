package com.test.fruitsample.service.model

import com.google.gson.annotations.SerializedName


data class Fruit(
        @SerializedName("type") val type: String,
        @SerializedName("price") val price: Int,
        @SerializedName("weight") val weight: Int
)

