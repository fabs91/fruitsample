package com.test.fruitsample.service

import com.test.fruitsample.analytics.AnalyticsReporter
import com.test.fruitsample.service.model.Fruit
import com.test.fruitsample.service.net.FruitsApiService
import io.reactivex.Single
import javax.inject.Inject

class FruitsServiceImpl @Inject constructor(private val fruitsApiService: FruitsApiService, private val analyticsReporter: AnalyticsReporter) : FruitsService {

    override fun getFruits(): Single<List<Fruit>> {
        return fruitsApiService.getFruits()
                .doOnSubscribe {
                    analyticsReporter.initEvent()
                }
                .map {
                    it.fruits
                }
                .doOnSuccess {
                    analyticsReporter.sendLoadEvent()
                }
                .doOnError {
                    analyticsReporter.sendErrorEvent(it.message.orEmpty())
                }
    }

}