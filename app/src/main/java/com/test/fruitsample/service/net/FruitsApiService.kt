package com.test.fruitsample.service.net

import android.support.annotation.NonNull
import com.test.fruitsample.di.qualifier.IoScheduler
import com.test.fruitsample.di.qualifier.MainScheduler
import com.test.fruitsample.service.net.response.FruitsReponse
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.SingleTransformer
import javax.inject.Inject

class FruitsApiService @Inject constructor(private val apiService: FruitsApiRetrofitService,
                                           @IoScheduler private val ioScheduler: Scheduler,
                                           @MainScheduler private val uiScheduler: Scheduler) {

    fun getFruits(): Single<FruitsReponse> {
        return apiService.getFruits()
                .compose(wrapExceptions())
                .compose(manageThreads())
    }

    fun sendStats(event: String, data: String): Completable {
        return apiService.sendStats(event, data)
                .subscribeOn(ioScheduler)
                .doOnError {
                    it.printStackTrace()
                }
                .onErrorComplete()
    }

    @NonNull
    private fun <T> wrapExceptions(): SingleTransformer<T, T> {
        return SingleTransformer {
            it.onErrorResumeNext({ throwable -> Single.error(throwable) })
        }
    }

    private fun <T> manageThreads(): SingleTransformer<T, T> {
        return SingleTransformer {
            it.subscribeOn(ioScheduler).observeOn(uiScheduler)
        }
    }

}