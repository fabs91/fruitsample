package com.test.fruitsample.service.net.response

import com.google.gson.annotations.SerializedName
import com.test.fruitsample.service.model.Fruit

data class FruitsReponse(@SerializedName("fruit") val fruits: List<Fruit>)