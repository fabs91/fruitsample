package com.test.fruitsample.analytics

import android.support.annotation.NonNull
import com.test.fruitsample.service.net.FruitsApiService
import com.test.fruitsample.util.TimeProvider
import javax.inject.Inject

class AnalyticsReporter @Inject constructor(private val timeProvider: TimeProvider, private val fruitsApiService: FruitsApiService) {

    companion object {
        val LOAD_EVENT = "load"
        val DISPLAY_EVENT = "display"
        val ERROR_EVENT = "error"
    }

    private var initialTime: Long = 0L

    fun initEvent() {
        initialTime = timeProvider.getTimeInMs()
    }

    fun sendLoadEvent() {
        if (isEventInitialized()) {
            val lastTime = timeProvider.getCurrentTimeDiff(initialTime)
            fruitsApiService.sendStats(LOAD_EVENT, lastTime.toString()).subscribe()
            resetInitialTime()
        }
    }

    fun sendDisplayEvent() {
        if (isEventInitialized()) {
            val lastTime = timeProvider.getCurrentTimeDiff(initialTime)
            fruitsApiService.sendStats(DISPLAY_EVENT, lastTime.toString()).subscribe()
            resetInitialTime()
        }
    }

    fun sendErrorEvent(@NonNull message: String) {
        fruitsApiService.sendStats(ERROR_EVENT, message).subscribe()
    }

    private fun isEventInitialized(): Boolean {
        return initialTime != 0L
    }

    private fun resetInitialTime() {
        initialTime = 0L
    }

}