package com.test.fruitsample.ui

import android.content.Intent
import com.test.fruitsample.details.FruitDetailsActivity
import com.test.fruitsample.di.activity.BaseActivity
import com.test.fruitsample.homepage.model.FruitModel
import javax.inject.Inject

class FruitNavigator @Inject constructor(val activity: BaseActivity) {

    companion object {
        const val DETAILS_REQUEST = 1
    }

    fun navigateToFruitDetail(fruitModel: FruitModel) {
        val intent = Intent(activity, FruitDetailsActivity::class.java)
        intent.putExtra(FruitDetailsActivity.EXTRA_MODEL, fruitModel)
        activity.startActivityForResult(intent, DETAILS_REQUEST)
    }

}
