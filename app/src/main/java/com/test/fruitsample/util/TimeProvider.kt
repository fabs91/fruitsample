package com.test.fruitsample.util

import android.support.annotation.NonNull

class TimeProvider {

    fun getTimeInMs(): Long {
        return System.currentTimeMillis()
    }

    fun getCurrentTimeDiff(@NonNull initialTimeMs: Long): Long {
        return System.currentTimeMillis() - initialTimeMs
    }

}