package com.test.fruitsample.homepage.model

import android.os.Parcel
import android.os.Parcelable

data class FruitModel(val type: String, val price: Int, val weight: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeInt(price)
        parcel.writeInt(weight)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FruitModel> {
        override fun createFromParcel(parcel: Parcel): FruitModel {
            return FruitModel(parcel)
        }

        override fun newArray(size: Int): Array<FruitModel?> {
            return arrayOfNulls(size)
        }
    }
}
