package com.test.fruitsample.homepage

import com.test.fruitsample.di.activity.ActivityComponent
import com.test.fruitsample.di.activity.ActivityScope
import dagger.Subcomponent


@ActivityScope
@Subcomponent(modules = [(HomePageModule::class)])
interface HomePageComponent : ActivityComponent {
    fun inject(activity: HomePageActivity)
}