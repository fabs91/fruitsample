package com.test.fruitsample.homepage

import com.test.fruitsample.di.activity.ActivityModule
import dagger.Module


@Module
class HomePageModule(activity: HomePageActivity) : ActivityModule(activity)
