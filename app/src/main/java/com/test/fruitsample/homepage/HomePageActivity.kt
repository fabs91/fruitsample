package com.test.fruitsample.homepage

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.test.fruitsample.FruitSampleApp
import com.test.fruitsample.R
import com.test.fruitsample.analytics.AnalyticsReporter
import com.test.fruitsample.di.activity.ActivityComponent
import com.test.fruitsample.di.activity.BaseActivity
import com.test.fruitsample.ui.FruitNavigator
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HomePageActivity : BaseActivity() {

    private val subscriptions: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var homePageViewModel: HomePageViewModel

    @Inject
    lateinit var navigator: FruitNavigator

    @Inject
    lateinit var analyticsReporter: AnalyticsReporter

    lateinit var recyclerView: RecyclerView
    lateinit var adapter: HomePageViewAdapter
    lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = HomePageViewAdapter()

        recyclerView = findViewById(R.id.rv_fruits_list)
        progressBar = findViewById(R.id.progress_bar)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(dividerItemDecoration)

        adapter.clickEventSubject.subscribe {
            analyticsReporter.initEvent()
            navigator.navigateToFruitDetail(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = getMenuInflater()
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    private fun displayProgress(visible: Boolean) {
        when (visible) {
            true -> progressBar.visibility = View.VISIBLE
            false -> progressBar.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.refresh -> {
                refreshData()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    private fun refreshData() {
        adapter.clear()
        displayProgress(true)

        subscriptions.add(homePageViewModel.getFruits().subscribe(
                {
                    adapter.update(it)
                    displayProgress(false)
                },
                {
                    processError(it)
                    displayProgress(false)
                }
        ))
    }

    override fun onStart() {
        super.onStart()

        refreshData()
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }

    private fun processError(error: Throwable) {
        Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
    }

    override fun createActivityComponent(application: FruitSampleApp): ActivityComponent {
        return application.createHomePageComponent(this)
    }

    override fun injectDependencies(component: ActivityComponent?) {
        (component as HomePageComponent).inject(this)
    }
}
