package com.test.fruitsample.homepage

import com.test.fruitsample.homepage.model.FruitModel
import com.test.fruitsample.service.FruitsService
import com.test.fruitsample.service.model.Fruit
import io.reactivex.Single
import javax.inject.Inject

class HomePageViewModel @Inject constructor(private val fruitsService: FruitsService) {

    fun getFruits(): Single<MutableList<FruitModel>> {
        return fruitsService.getFruits()
                .map(this::populateFruitModels)
    }

    private fun populateFruitModels(fruitList: List<Fruit>): MutableList<FruitModel> {
        val models: MutableList<FruitModel> = mutableListOf()
        for (fruit: Fruit in fruitList) {
            models.add(FruitModel(fruit.type, fruit.price, fruit.weight))
        }
        return models
    }

}