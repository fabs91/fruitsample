package com.test.fruitsample.homepage

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jakewharton.rxbinding2.view.RxView
import com.test.fruitsample.R
import com.test.fruitsample.homepage.model.FruitModel
import io.reactivex.subjects.PublishSubject

class HomePageViewAdapter : RecyclerView.Adapter<HomePageViewAdapter.ViewHolder>() {

    private val data = mutableListOf<FruitModel>()

    val clickEventSubject: PublishSubject<FruitModel> = PublishSubject.create()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fruits_list_item, parent, false)
        val holder = ViewHolder(view)

        RxView.clicks(view)
                .takeUntil(RxView.detaches(parent))
                .map<FruitModel> { data.get(holder.layoutPosition) }
                .subscribe(clickEventSubject)
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fruitModel: FruitModel = data.get(position)
        holder.bind(fruitModel)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun update(data: List<FruitModel>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var tvFruitName: TextView

        init {
            tvFruitName = itemView.findViewById(R.id.tv_fruit_name)
        }

        fun bind(fruitModel: FruitModel) {
            tvFruitName.setText(fruitModel.type)
        }
    }

    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }
}