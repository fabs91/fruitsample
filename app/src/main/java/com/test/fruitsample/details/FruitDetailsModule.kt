package com.test.fruitsample.details

import com.test.fruitsample.di.activity.ActivityModule
import dagger.Module


@Module
class FruitDetailsModule(activity: FruitDetailsActivity) : ActivityModule(activity)