package com.test.fruitsample.details

import com.test.fruitsample.di.activity.ActivityComponent
import com.test.fruitsample.di.activity.ActivityScope
import dagger.Subcomponent


@ActivityScope
@Subcomponent(modules = [(FruitDetailsModule::class)])
interface FruitDetailsComponent : ActivityComponent {
    fun inject(activity: FruitDetailsActivity)
}