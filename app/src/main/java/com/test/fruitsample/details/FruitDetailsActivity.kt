package com.test.fruitsample.details

import android.os.Bundle
import android.widget.TextView
import com.test.fruitsample.FruitSampleApp
import com.test.fruitsample.R
import com.test.fruitsample.analytics.AnalyticsReporter
import com.test.fruitsample.di.activity.ActivityComponent
import com.test.fruitsample.di.activity.BaseActivity
import com.test.fruitsample.homepage.model.FruitModel
import javax.inject.Inject


class FruitDetailsActivity : BaseActivity() {

    companion object {
        val EXTRA_MODEL = "EXTRA_MODEL"
    }

    @Inject
    lateinit var analyticsReporter: AnalyticsReporter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details_fruits)

        val model = intent.getParcelableExtra<FruitModel>(EXTRA_MODEL)

        if (model != null) {
            findViewById<TextView>(R.id.tv_fruit_name).text = model.type
            findViewById<TextView>(R.id.tv_fruit_price).text = getString(R.string.price, model.price / 100, model.price % 100)
            findViewById<TextView>(R.id.tv_fruit_weight).text = getString(R.string.weight, model.weight / 1000f)
        }
    }

    override fun onStart() {
        super.onStart()

        analyticsReporter.sendDisplayEvent()
    }

    override fun createActivityComponent(application: FruitSampleApp): ActivityComponent {
        return application.createFruitDetailsComponent(this)
    }

    override fun injectDependencies(component: ActivityComponent?) {
        (component as FruitDetailsComponent).inject(this)
    }
}
