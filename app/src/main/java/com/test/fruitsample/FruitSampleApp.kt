package com.test.fruitsample

import android.app.Application
import com.test.fruitsample.analytics.AnalyticsReporter
import com.test.fruitsample.details.FruitDetailsActivity
import com.test.fruitsample.details.FruitDetailsComponent
import com.test.fruitsample.details.FruitDetailsModule
import com.test.fruitsample.di.application.ApplicationComponent
import com.test.fruitsample.di.application.ApplicationModule
import com.test.fruitsample.di.application.DaggerApplicationComponent
import com.test.fruitsample.homepage.HomePageActivity
import com.test.fruitsample.homepage.HomePageComponent
import com.test.fruitsample.homepage.HomePageModule
import javax.inject.Inject

/**
 * Abstract application class contains logic to manage the dependency graph
 */
class FruitSampleApp : Application(), InjectableApplication {

    private lateinit var applicationComponent: ApplicationComponent

    @Inject
    lateinit var analyticsReporter: AnalyticsReporter

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
        applicationComponent.inject(this)

        Thread.setDefaultUncaughtExceptionHandler({ thread, t ->
            analyticsReporter.sendErrorEvent(t.message.orEmpty())
        })
    }

    override fun createHomePageComponent(homePageActivity: HomePageActivity): HomePageComponent {
        return applicationComponent.plusHome(HomePageModule(homePageActivity))
    }

    override fun createFruitDetailsComponent(detailsFruitActivity: FruitDetailsActivity): FruitDetailsComponent {
        return applicationComponent.plusDetails(FruitDetailsModule(detailsFruitActivity))
    }
}